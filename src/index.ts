/**
 * File: /src/index.ts
 * Project: multiplatform.one
 * File Created: 09-11-2022 08:55:55
 * Author: Clay Risser
 * -----
 * Last Modified: 23-02-2023 06:06:54
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2021 - 2022
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { MultiPlatform } from './multiplatform';

export { MultiPlatform };

export * from './config';
export * from './helpers';
export * from './hooks';
