/**
 * File: /recoil/index.d.ts
 * Project: multiplatform.one
 * File Created: 01-02-2023 14:09:52
 * Author: Clay Risser
 * -----
 * Last Modified: 01-02-2023 14:13:36
 * Modified By: Clay Risser
 * -----
 * Risser Labs LLC (c) Copyright 2022 - 2023
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import type { AtomEffect } from 'recoil-persist/dist';

export declare const persistAtom: AtomEffect<any>;
